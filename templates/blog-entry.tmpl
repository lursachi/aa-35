{#
 # This file is part of acceptableads.org.
 # Copyright (C) 2017-present eyeo GmbH
 #
 # acceptableads.org is free software: you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation, either version 3 of the License, or
 # (at your option) any later version.
 #
 # acceptableads.org is distributed in the hope that it will be useful,
 # but WITHOUT ANY WARRANTY; without even the implied warranty of
 # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # GNU General Public License for more details.
 #
 # You should have received a copy of the GNU General Public License
 # along with acceptableads.org.  If not, see <http://www.gnu.org/licenses/>.
 #}

{% extends "templates/default" %}
{% if not og_image and featured_img_url %}
  {% set og_image = featured_img_url %}
{% endif %}
{% block head %}
  <meta property="og:article:published_time" content="{{ published_date }}">
  <meta name="author" content="{{ author }}">
{% endblock %}
{% block blog %}
  <article role="main">
    <h1>{{ title }}</h1>
    <div class="row">
      <div class="col-6 push-6">
        <img
            id="featured-image"
            class="block"
            src="{{ featured_img_url }}"
            alt="{{ featured_img_alt }}">
      </div>
      <div class="col-6 pull-6 m-b-xl">
        <p>
          <b>
            POSTED
            <time datetime="{{ published_date }}">
              {{ humanize_date(published_date) }}
            </time>
          </b>
          <span class="muted">
            by {{ author }}
          </span>
        </p>
        {{ super() }}
      </div>
    </div>
  </article>
{% endblock %}
