<div class="card" markdown="1">

<header class="card-heading" markdown="1">
### {{abp-safary-heading[Acceptable Ads SUPPORTED PRODUCTS > heading] <fix>Adblock Plus</fix> for <fix>Safari</fix> <fix>iOS</fix> }}
</header>

{{abp-safary-body[Acceptable Ads SUPPORTED PRODUCTS > body] Content blocker for <fix>Safari</fix> on <fix>iOS</fix> versions 8 and higher }}
{: .card-summary }

<footer class="card-footer" markdown="1">
[{{download[Text for download button] Download}}](https://itunes.apple.com/app/adblock-plus-abp/id1028871868){: .btn-outline-primary }
</footer>

</div>