## {{what-are-aa-heading[What are Acceptable Ads heading] What are Acceptable Ads? }}

{{what-are-aa-summary[What are Acceptable Ads summary] Acceptable Ads must comply with specific criteria to be shown to users of ad-blocking software. }}
{: .m-b-lg }

<div class="group group-3 items">
  <? include index/what-are-aa/placement ?>
  <? include index/what-are-aa/distinction ?>
  <? include index/what-are-aa/size ?>
</div>
