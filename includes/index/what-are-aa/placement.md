
<div class="item" markdown="1">

### {{placement-heading[What are Acceptable Ads > 01. Placement heading] Placement }}

{{placement-body[What are Acceptable Ads > 01. Placement body] Ads should not disrupt the user’s natural reading flow. Ads should instead be placed above, beside or below the primary content. }}
{: .item-summary }

[{{placement-learn-more[What are Acceptable Ads > 01. Placement learn more button] Learn more }}](about/criteria#placement){: .btn-outline-primary }


</div>
