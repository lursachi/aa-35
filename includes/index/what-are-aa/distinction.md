
<div class="item" markdown="1">

### {{distinction-heading[What are Acceptable Ads > 02. Distinction heading] Distinction }}

{{distinction-body[What are Acceptable Ads > 02. Distinction body] Ads should be clearly marked with the word "advertisement" or a reasonable equivalent. }}
{: .item-summary }

[{{distinction-learn-more[What are Acceptable Ads > 02. Distinction learn more button] Learn more }}](about/criteria#distinction){: .btn-outline-primary }

</div>
