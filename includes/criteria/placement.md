
{{placement-criteria[Paragraph in "Placement" section] Ads must not disrupt the user's natural reading flow. Such ads must be placed on top, side or below the Primary Content.}} [^1]

[^1]: {{primary-content[footnote in "Other Acceptable Ads formats"] The 'Primary Content' is defined as (based on <a
href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/main">Mozilla's description</a> of the <main\> HTML element): The Primary Content consists of content that is directly related to, or expands upon the central topic of a document or the central functionality of an application. This content should be unique to the document, excluding any content that is repeated across a set of documents such as sidebars, navigation links, copyright information, site logos, and search forms (unless, of course, the document's main function is a search form).}}
