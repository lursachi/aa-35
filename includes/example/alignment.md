
### Alignment

This text is aligned <abbr title="End Of Line">eol</abbr>
{: .eol .m-y-sm .p-a-sm }

This text is aligned center
{: .center .m-y-sm .p-a-sm }
