
This is an extra large block
{: .xl .bg-info }

This is a large block
{: .lg .bg-info }

This is a medium block
{: .md .bg-info }

This is a small block
{: .sm .bg-info }

This is an extra small block
{: .xs .bg-info }
