
### Buttons

[Outline primary](#){: .btn-outline-primary }

[Outline accent](#){: .btn-outline-accent }

[Solid secondary](#){: .btn-primary }

[Solid accent](#){: .btn-accent }
