
### Backgrounds

Secondary background
{ .bg-secondary .p-a-sm .m-y-sm }

Accent background
{ .bg-accent .p-a-sm .m-y-sm }

Error background
{: .bg-error .p-a-sm .m-y-sm }

<small>These *secondary*{: .bg-secondary }, *accent*{: .bg-accent }, and *error*{: .bg-error } styles also work inline.</small>
