# {{acceptable-ads-tool[Acceptable Ads Tool Heading] <fix>Acceptable Ads</fix> Certification Tool}} <small>({{beta-label[beta label placed after heading] beta}})</small>

---

**{{reach-millions[Acceptable Ads Tool Subheading] Reach millions of ad-blocking users with pre-approved ads and one-tag implementation in minutes.}}**

{{ads-dont-have-to-be-annoying[Acceptable Ads Tool paragraph 2] Ads don’t have to be annoying - even if you’re pushing for increased engagement, viewability and yield. Millions of users have installed ad blockers in what could be the greatest consumer revolt of all time; there is a way to win those picky users back with nonintrusive ads.}}

{{adblock-plus-getting-users-back[Acceptable Ads Tool paragraph 3] Adblock Plus has been getting these users back with the Acceptable Ads initiative. The secret of the initiative is trust: users don’t have to take part, but because it’s on their own terms, most do. And yet, after over five years of existence, it was still a labor-intensive process for publishers and advertisers to join.}}

{{acceptabl-ads-tool-wins[Acceptable Ads Tool paragraph 4] Not anymore. The Acceptable Ads Certification Tool (AACT) is an open infrastructure that validates Acceptable Ads and facilitates them being served using tools common to the ad tech industry.}}

## {{easy-implementation[Easy Implementation Heading] Easy implementation optimized for your audience}}

{{see-pre-approved-ads[Easy Implementation paragraph 1] You can use pre-approved ads from various demand channels, or you can use your own and let the AACT check them in real-time against the Acceptable Ads criteria. The implementation is simple, intuitive and includes a unique user feedback mechanism, which lets you optimize your ads for your specific user base.}}

[{{sign-up-label[Sign Up button label] Sign Up}}](mailto:r.dar@eyeo.com){: .btn-primary target="blank" }
{: .btn-container }
