
<div class="card" markdown="1">

<header class="card-heading" markdown="1">
![{{advertisers-icon-alt[Advertisers icon alt text] Publishers icon}}](/img/png/icon-advertisers.png)
{: .card-icon }

## {{advertisers-heading[Advertisers heading] Advertisers}}
</header>

<div class="card-summary" markdown="1">

{{advertisers-p1[Advertisers paragraph 1] Unlock an important target group}}

{{advertisers-p2[Advertisers paragraph 2] Learn more about how to reach the relevant audience of adblock users in a respectful way}}

</div>

<footer class="card-footer" markdown="1">
[{{advertisers-learn-more-button[Advertisers learn more button] Learn more}}](solutions/advertisers){: .btn-outline-primary }

[{{get-whitelisted-button[Get whitelisted button] Get whitelisted}}](get-whitelisted?type=advertisers){: .btn-primary }
</footer>

</div>
