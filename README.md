
# acceptableads.org

Source code and content of [acceptableads.org](https://acceptableads.org).

## License

- Source code: [GPL-3.0](https://www.gnu.org/licenses/gpl.html)
- Content: [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/)
