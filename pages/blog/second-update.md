title=First AAC meeting date is set; plus, new members and the search for the golden user
description=The first official Acceptable Ads Committee meeting will take this summer in New York City. In the meantime, we continue to search for a user representative.
featured_img_url=/img/png/blog/acceptable-ads-committee-update.png
featured_img_alt=Acceptable Ads Committee update
author=Ben Williams
committee=true
published_date=2017-04-12
template=blog-entry
breadcrumb=Second update
parent=blog/index

We announced the inaugural Acceptable Ads Committee (AAC) about a month ago. Since we published [our last update](https://acceptableads.com/en/blog/first-update), the committee has added a few new members and continued our search for the special place at the table that is reserved for a “normal” user.

Did I say “normal”? Well, that’s a pretty poor descriptor for any of the candidates we’ve interviewed so far. Sure, they can represent the interests of millions of “normal” users, like me for instance; and yeah, they’re “normal” in the sense that they don’t work in the ad industry (cuz let’s face it, most people don’t); but they’re definitely NOT “normal” in how knowledgeable, thoughtful or just plain awesome they are. On the contrary: like the position they’re taking – which has never been offered to anyone unaffiliated with the ads industry in the history of the ads industry – there’s nothing “normal” about them.

We have no one to announce as the user representative just yet, but stay tuned for an announcement very soon. Meanwhile, the applications keep streaming in …

Beyond our quest to find this extraordinary user, we’ve been tracking down whip-smart, forward-thinking people in and around the ads industry to fill more slots on the AAC. As expected, there have been a few shake-ups and additions in the last couple weeks or so.

- The first AAC meeting will be in New York on July 11!
- Gavin Hall of TED Talks has decided to take over the leading role in the Publishers and Content Creators member group from Kenton Jacobsen of Conde Nast; Kenton has decided to leave because of previous commitments and the time constraints of serving turned out to be too demanding to make the position tenable. We will miss him, but we are excited to welcome Gavin!
- Tobias Eidem of BrandGlobe and former chairman of Sweden’s KIA-index, joined the Expert member group!
- Loic Dussart, formerly at Adap.tv or Virool, now at brand new video content platform KOL as Chief Opreation Officer, joined the Publishers and Content Creators member group!

Next time we’ll announce that golden, certainly-not-normal user. In the meantime, [sign yourself up to write the next page in online advertising!](https://acceptableads.com/en/committee/apply)
