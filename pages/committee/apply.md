title=Apply to join | The Acceptable Ads Committee
description=The Acceptable Ads Committee has full control over the Acceptable Ads initiative, including what’s deemed acceptable by defining the Acceptable Ads criteria.
parent=committee/index

<style type="text/css">
  #success-msg, #error-msg, .success #clear, .success #submit
  {
    display: none;
  }

  .success #success-msg, .error-response #error-msg
  {
    display: block;
  }
</style>

<script type="text/javascript">
function addListener(obj, type, listener, useCapture)
{
  if ("addEventListener" in obj)
  {
    obj.addEventListener(type, function(ev)
    {
      if (listener(ev) === false)
        ev.preventDefault();
    }, useCapture);
  }
  else
  {
    // IE 8
    if (type == "DOMContentLoaded")
      type = "readystatechange";

    type = "on" + type;
    if ("attachEvent" in obj)
    {
      obj.attachEvent(type, function()
      {
        if (document.readyState == "complete" && listener(event) === false)
          event.returnValue = false;
      });
    }
    else
    {
      obj[type] = listener;
    }
  }
}

function contentLoad()
{
  addListener(document.getElementById("application-form"), "submit", function()
  {
    var formElement = document.getElementById("application-form");

    var params = "";
    var fields = formElement.elements;
    for (var i = 0; i < fields.length - 2; i++)
      params += fields[i].name + "=" + encodeURIComponent(fields[i].value) + "&";

    params = params.slice(0, -1);

    var request = new XMLHttpRequest();
    request.open("POST", "/committee/apply/submit", true);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    addListener(request, "readystatechange", function()
    {
      if (request.readyState == 4)
      {
        if (request.status >= 200 && request.status < 300)
        {
          formElement.setAttribute("class", "success");
        }
        else
        {
          var errorWrapper = document.getElementById("error-msg");
          if ("textContent" in errorWrapper)
            errorWrapper.textContent = request.statusText;
          else // IE8
            errorWrapper.innerText = request.statusText;

          formElement.setAttribute("class", "error-response");
        }
      }
    }, false);
    request.send(params);
    return false;
  }, false);
}

addListener(document, "DOMContentLoaded", contentLoad, false);
</script>

<div class="row" markdown="1">
<div class="col-6 expand-on-tablet" markdown="1">
<form id="application-form" class="section" method="POST" markdown="1">

# {{join-committee-heading[Headline text] Apply to join the committee}}

<div class="section" markdown="1">
{{join-committee-paragraph[Join committee paragraph] Do you want to help create a new standard of advertising and improve the ad experience for millions of ad-blocking users? If so, fill out the form below!}}
</div>

  <label for="name">
    {{name[Field label text] Name}}
  </label>
  <input id="name" type="text" name="name" required />

  <label for="title">
    {{title[Field label text] Title}}
  </label>
  <input id="title" type="text" name="title" required />

  <label for="company">
    {{company[Field label text] Company / Organization}}
  </label>
  <input id="company" type="text" name="company" required />

  <label for="email">
    {{email[Field label text] Email address}}
  </label>
  <input id="email" type="email" name="email" required />

  <label for="phone">
    {{phone[Field label text] Phone number}} *{{optional[Optional field label] (optional)}}*
  </label>
  <input id="phone" type="tel" name="phone" />

  <label for="group">
    {{group[Field label text] Stakeholder group}}
  </label>
  <select id="group" name="group">
    <option selected="selected" value="Advertiser">{{advertisers[Item inside Stakeholder group selectbox] Advertiser}}</option>
    <option value="Ad-blocker user">{{ad-blocker-user[Item inside Stakeholder group selectbox] Ad-blocker user}}</option>
    <option value="Advertising Agency">{{ad-agencies[Item inside Stakeholder group selectbox] Advertising Agency}}</option>
    <option value="Publisher/Content-Creator">{{publishers[Item inside Stakeholder group selectbox] Publisher/Content-Creator}}</option>
    <option value="Digital Rights Organization">{{digital-rights-org[Item inside Stakeholder group selectbox] Digital Rights Organization}}</option>
    <option value="User Agent">{{user-agent[Item inside Stakeholder group selectbox] User Agent}}</option>
    <option value="Researcher">{{researcher[Item inside Stakeholder group selectbox] Researcher}}</option>
    <option value="Designer">{{designer[Item inside Stakeholder group selectbox] Designer}}</option>
    <option value="Ad tech agency">{{ad-tech-agency[Item inside Stakeholder group selectbox] Ad tech agency}}</option>
    <option value="Other">{{other[Item inside Stakeholder group selectbox] Other}}</option>
  </select>

  <label for="motivation">
    {{motivation[Field label text] Motivation}}
  </label>
  <textarea id="motivation" placeholder="{{text-area-placeholder[Placeholder text inside textarea] Please provide in 100 words or less a short description of why you would be a good fit}}" rows="14" name="motivation" required></textarea>

  <input id="clear" class="btn-outline-primary" type="reset" name="reset" value="{{submit Clear}}" />
  <input id="submit" class="btn-primary" type="submit" name="Submit" value="{{submit Submit}}" />
  {: .eol .btn-container }

  {{submition-success[Text to be shown after successfull submition] Your entry has been successfully submitted. Someone will reach out to you within 10 working days.}}
  { #success-msg .bg-accent .p-a-sm .m-y-sm }

  error
  {: #error-msg .bg-error .p-a-sm .m-y-sm }

</form>
</div>
</div>
