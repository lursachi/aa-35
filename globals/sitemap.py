# This file is part of acceptableads.org.
# Copyright [C] 2016-present eyeo GmbH
#
# acceptableads.org is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# [at your option] any later version.
#
# acceptableads.org is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with acceptableads.org.  If not, see <http://www.gnu.org/licenses/>.

# Used to generate #primary-navigation and #breadcrumbs
#     level 1 - ["$PAGE_NAME", "$PAGE_TITLE", [$PAGE_CHILDREN]]
#     level 2 - ["$PAGE_NAME", "$PAGE_TITLE"]

sitemap = [
    ["about/index", "<span class='no-wrap-mobile'>What are <fix>Acceptable Ads</fix>?</span>", [
        ["about/criteria", "Criteria"],
        ["about/best-practices", "Best Practices"],
    ]],
    ["solutions/index", "Solutions",[
        ["solutions/publishers", "Publishers"],
        ["solutions/ad-networks", "Ad Networks"],
        ["solutions/advertisers", "Advertisers"],
        ["solutions/ad-tech-suppliers", "Ad-Tech Suppliers"],
    ]],
    ["get-whitelisted/index", "Get whitelisted", []],
    ["users", "Users", []],
    ["committee/index", "Committee", [
        ["committee/apply", "Apply"],
        ["committee/members", "Members"],
        ["committee/documents", "Documents"],
        ["https://blog.acceptableads.com/", "Committee Blog"],
    ]],
    ["blog/index", "Blog", []],
]
